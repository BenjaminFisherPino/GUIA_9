// Codigo extraido del ejercicio 1 de la guia 3 U1
// No me quedo muy claro como implementar el algoritmo
// visto en clase. Esto ofrece una solucion, pero
// no es lo que se habia propuesto como solucion.

#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista() {}

void Lista::agregar(int numero){
    Nodo *q;

    //Se crea un nodo.
    q = new Nodo;

    //Se asigna el nodo al objeto numero.
    q->numero = numero;
    
    //Se inicializa como NULL por defecto.
    q->sig = NULL;

    /*Si el es primer nodo de la lista, lo deja
    como raíz y como último nodo.*/
    if (this->principal == NULL) { 
		this->principal = q;

    /*De lo contrario, apunta el actual último nodo 
    al nuevo y deja el nuevo como el último de la lista. */
    }
    
    else {
		Nodo *s, *r, tmp;
		s = this->principal;
		
        bool verificador = true;

		while ((s != NULL) && (s->numero < numero)){

			r = s;
			s = s->sig;
		}
		
		if (s == this->principal){
			this->principal = q;
			this->principal->sig = s;
		}
		
		else {
			r->sig = q;
			q->sig = s;
		}
  }
}

void Lista::buscar(int numero){
    Nodo *q;

    //Se crea un nodo.
    q = new Nodo;

    //Se asigna el nodo al objeto numero.
    q->numero = numero;
    
    //Se inicializa como NULL por defecto.
    q->sig = NULL;

    /*Si el es primer nodo de la lista, lo deja
    como raíz y como último nodo.*/
    if (this->principal->numero == numero) { 
        cout << "La informacion se encuntra en la lista de nodos" << endl;

    /*De lo contrario, apunta el actual último nodo 
    al nuevo y deja el nuevo como el último de la lista. */
    }
    
    else {
		Nodo *s, *r, tmp;
		s = this->principal;
		
		while ((s != NULL) && (s->numero != numero)){
			r = s;
			s = s->sig;
		}
		
		if (s == NULL){
        cout << "La informacion no esta en la lista de nodos." << endl;
		} else{
        cout << "la informacion se encuentra en la lista de nodos." << endl;
		}
  }
}

void Lista::imprimir (){
	
    //Utiliza variable temporal para recorrer la lista.
    Nodo *q = this->principal;
    
    //Contador
	int i = 1;
	
    //La recorre mientras sea distinto de NULL (no hay más nodos).
    while (q != NULL) {
        cout <<  i << ") " << q->numero << endl;
        i++;
        q = q->sig;
    }
    cout << endl;
}
