# Guia 8

**Versión 1.2.5**

Archivo README.md para guía numero 9 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamin Ignacio Fisher Pino

Fecha: 20/12/2021

---
## Resúmen del programa

Este programa fue hecho por Benjamin Fisher para almacenar y buscar data en un array/lista de nodos enlazados utilizando funciones Hash.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en el siguiente enlace.

Link: 

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe ingresar a la carpeta  y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto N "

→ EJEMPLO

"./proyecto 1"

N corresponde a el metodo para solucionar errores a utilizar, estos numeros van del 1 al 4. Las opciones son "Prueba lineal", "Prueba cuadratica", "Prueba de direccion" y "Encadenamiento" respectivamente. Es vital la utilizacion de este parametro para el funcionamiento del programa. ;)

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y  la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

El codigo funciona gracias a 9 archivos, un archivo principal (Main.cpp), el cual nos permite fijar una guia para la compilacion y ejecutacion del programa. Los otros 8 archivos corresponden a los 4 metodos de resolucion de colisiones (archivos.cpp) mas su srespectivos archivos modelo (archivos.h).

### Funcionamiento de los algoritmos utilizados

Hay dos funciones Hash utilizadas en este programa, "hash_modulo" y "new_hash_modulo".

Hash_modulo: Esta funcion nos permite generar una direccion en base a un numero ingresado por el usuario y el tamaño total del array. La funcion nos retorna el valor obtenido de la ecuacion "(numero % N) + 1", siendo "numero" el dato ingresado por el usuario y "N" el tamaño del array.

New_hash_modulo: Esta funcion solo se utiliza en la opcion 3 "Prueba de direccion". Sirve como funcion Hash complementadora, ya que primero se utiliza hash_modulo(), solo en el caso de que no se pueda insertar o encontrar el elemento emn la primera busqueda se recurre a esta nueva funcion. La funcion nos retorna el valor obtenido de la ecuacion "((D + 1) % 10) + 1", siendo "D" el resultado de la funcion hash anterior (hash_modulo). Ademas, despues de la primera vez de utilizada esta funcion, se cambiara a new_hash_modulo(DX), siendo "DX" el resultado obtenido en la primera utilizacion de la funcion nueva.

Hablando de los metodos de resolucion de colisiones, hay 8 funciones, 4 para insertar elementos y 4 para buscar elementos.

Las funciones para buscar elementos funcionan de forma similar a excepcion de la opcion 4 (sera explicada al final). Primero se pregunta si el numero ingresado por el usuario coincide con la direccion Hash generada. En caso de ser cierto, se encontro el numero, en caso contrario, se busca recursivamente dentro de todo el array hasta encontrarlo o llegar al punto de partida. La opcion 4 funciona un poco diferente, se busca recursivamente en la lista de nodos hasta encontrar el elemento o hasta que se acabe la lista.

Las funciones para ingresar elementos tambien funcionan de forma parecida, nuevamente la opcion 4 es una excepcion. Primero se pregunta si la direccion generada (en base a un numero ingresado por el usuario) esta vacia, de ser asi, se le asigna el valor del usuario, en caso contrario, se busca recursivamente un espacio vacio dentro del array hasta encontrar uno o llegar al fin del array (lo que significa que todos los otros espacios estan utilizados). La opcion 4 funciona un poco diferente, solo se añade el numero ingresado por el usuario, sin la necesidad de funciones Hash ni otros metodos, todo esto debido a la naturaleza de las listas de nodos anidados y su capacidad de generar memoria dinamica.

### Funcionamiento del programa

Tras iniciar el programa se nos pedira ingresar las dimensiones del array, luego de esto, se le pedira al usuario ingresar una de dos opciones. En caso de escoger la 4 opcion, se recomienda dejar el tamaño como 0, ya que, no se utilizara el array.

La primera opcion corresponde a "Agregar numero", la cuial nos permite utilizar una clave ingresada por el usuario, transformarla en una direccion mediante funciones Hash y finalmente ingresar el elemento en el array. En el caso de la opcion 4, solo se añadira el numero a la lista de nodos enlazados, sin la necesidad de generar una direccion en base a una funcion Hash.

La segunda opcion, "Buscar elemento", nos permite buscar un elemento ingresado por el usuario. Esto se logra utilizando la funcion de prueba lineal para buscar la direccion de una clave en el array. En el caso de la opcion 4, se busca recursivamente dentro de la lista de nodos hasta encontrar el elemento deseado.

En cada ciclo se le preguntara al usuario si quiere continuar con la ejecucion del programa, 1 para continuar, 2 para finalizar.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

Contacto
→ Mail: bfisher20@alumnos.utalca.cl

