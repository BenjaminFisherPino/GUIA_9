#include <iostream>
using namespace std;

#include "Cuadratica.h"

/*CONSTRUCTOR*/
Cuadratica::Cuadratica(){}

/*FUNCION HASH*/
int Cuadratica::hash_modulo(int num, int n){
    int clave = (num % n);
    return clave;
}

/*BUSCAR UN NUMERO*/
void Cuadratica::prueba_cuadratica_buscar(int num, int n, int *array){

    int D, DX, I;
    D = hash_modulo(num, n);

    // Si la informacion se encuentra en la posicion indicada por 
    // la funcion hash... En caso contrario se procede a buscar
    // dentro de todo el array.
    if ((array[D] != 0) && (array[D]) == num){
        cout << "La informacion esta en la posicion " << D + 1 << endl;
    } else{

        I = 1;
        DX = (D + (I * I));

        // Ciclo que nos permite buscar el elemento deseado en el array.
        while ((array[DX] != 0) && (array[DX] != num)){
            I++;
            DX = (D + (I * I));
            if (DX > n){
                I = 0;
                DX = 1;
                D = 1;
            }
        }

        // Si el espacio esta vacio.
        if (array[DX] == 0){
            cout << "La informacion no se encuentra en el array" << endl;

        // En caso contrario, el numero se encuntra en la posicion DX.
        // Se le suma 1 por temas de estetica(1 = 0(posicion) + 1).
        } else{
            cout << "La informacion se encuentra en la posicion " << DX + 1 << endl;
        }
    }
}

/*INGRESAR UN NUMERO*/
void Cuadratica::prueba_cuadratica_insertar(int num, int n, int *array){

    int D, DX, I;
    D = hash_modulo(num, n);

    // Si la posicion indicada por la funcion Hash esta vacia se inserta el numero.
    // En caso contrario, se empieza a buscar en el array algun lugar vacio.
    if (array[D] == 0){
        array[D] = num;
    } else{
        cout << "El espacio " << D + 1 << " esta ocupado por el numero " << array[D] << endl;

        // Cuando ingresamos un numero que ya esta en el array, la funcion Hash nos va a 
        // arrojar la misma direccion. Por esto, se verifica de que el numero ingresado sea
        // distinto del numero con el cual tuvo una colision, de este modo, no se repiten los
        // los numeros en el array.
        bool verificador = false;

        /*VERIFICADOR*/
        if (num == array[D]){
            verificador = true;
        }

        I = 1;
        DX = (D + (I * I));

        // Ciclo que nos permite buscar un espacio vacio en el array.
        while ((array[DX] != 0) && (array[DX] != num) && (DX != D) && (!verificador)){

            I++;
            DX = (D + (I * I));

            /*VERIFICADOR*/
            if (num == array[DX]){
                verificador = true;
            }

            if (DX > n){
                I = 0;
                DX = 1;
                D = 1;
            }
        }

        // Si se encuentra un lugar vacio...
        if ((array[DX] == 0) && (DX < n)){

            // Si el numero se repite.
            if (verificador){
                cout << "La clave ya se encuentra dentro del array" << endl;

            // Caso contrario.
            } else{
                cout << "Se opto por localizar el numero " << num << " en la posicion " << DX + 1 << endl;
                array[DX] = num;
            }
        
        // Si no queda espacio en el array.
        } else{
            cout << "No queda espacio en el array" << endl;
        }
    }
}