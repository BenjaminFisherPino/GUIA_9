prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Main.cpp Lineal.cpp Cuadratica.cpp Direccion.cpp Lista.cpp
OBJ = Main.o Lineal.o Cuadratica.o Direccion.o Lista.o
APP = proyecto

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
