#ifndef DIRECCION_H
#define DIRECCION_H

class Direccion {

    /*ATRIBUTOS*/
    private:

    public:
    /*CONSTRUCTOR*/
		Direccion();

    /*METODOS*/
        int hash_modulo(int num, int n);
        int new_hash_modulo(int D);
        void prueba_direccion_buscar(int clave, int n, int *array);
        void prueba_direccion_insertar(int clave, int n, int *array);
};
#endif