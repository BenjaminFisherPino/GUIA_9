#include <iostream>
#include <fstream>

#include "Lineal.h"
#include "Cuadratica.h"
#include "Direccion.h"
#include "Lista.h"

using namespace std;

/*VERIFICADOR DEL TAMAÑO DEL ARRAY*/
int verificador(int n){
    while (n < 0){
        cin >> n;    
    }

    system("clear");
    return n;
}

/*IMPRIME ARRAY*/
void imprime_array(int *array, int n){

    int cont = 1;

    for (int i = 0; i < n; i++){

        cout << "[" << array[i] << "]"; 

        if (cont == 5){
            cout << endl;
            cont = 0;
        }

        cont++;
    }
}

/*FUINCION MAIN*/
int main(int argc, char **argv) {
    
    /*VARIABLES*/
    int opc, resp, n, num_temp, metodo;
    string message;

    Lineal L;
    Cuadratica C;
    Direccion D;
    Lista E;

    metodo = atoi(argv[1]); // Metodo a utilizar

    /* VALIDADOR DE PARAMETRO INICIAL*/
    while ((metodo < 0) && (metodo > 4)){
        cin >> metodo;
    }

    cout << "Ingrese el tamaño del array, en caso de utilizar la opcion 4" <<
            " ingrese un 0, de lo contrario ingrese un numero mayor a 0 " << endl;
    cout << "Tamaño: ";
    cin >> n;
    n = verificador(n);
    int array[n];

    /*INICIALIZACION DE VALORES DEL ARRAY*/
    for (int i = 0; i < n; i++){
        array[i] = 0;
    }

    /*VALIDACION DE METODO*/
    if (metodo == 1){
        message = "prueba lineal";
        Lineal L = Lineal();

    } else if (metodo == 2){
        message = "prueba cuadratica";
        Cuadratica C = Cuadratica();

    } else if (metodo == 3){
        message = "doble direccion";
        Direccion D = Direccion();

    } else{
        message = "encadenamiento";
        Lista E = Lista();
    }

    resp = 1;
    system("clear");
    cout << "Bienvenido!" << endl << endl;

    /*CICLO PRINCIPAL*/
    while (resp == 1){

        cout << "*****************************************" << endl;
        cout << "               MENU                      " << endl;
        cout << "                                         " << endl;
        cout << " METODO DE CORRECCION: " << message << " " << endl;
        cout << "                                         " << endl;
        cout << " 1) INGRESAR DATO                        " << endl;
        cout << " 2) BUSCAR DATO                          " << endl;
        cout << "*****************************************" << endl;
        cout << endl << endl;

        cin >> opc;

        if (opc == 1){ // ingresar un numero
            system("clear");

            if (metodo == 1){ // prueba lineal.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                L.prueba_lineal_insertar(num_temp, array, n);
                imprime_array(array,n);

            } else if (metodo == 2){ // prueba cuadratica.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                C.prueba_cuadratica_insertar(num_temp, n, array);
                imprime_array(array,n);

            } else if (metodo == 3){ // prueba de direccion.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                D.prueba_direccion_insertar(num_temp, n, array);
                imprime_array(array,n);
            } else{ // encadenamiento.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                E.agregar(num_temp);
                E.imprimir();

            }

        } else if (opc == 2){ //Buscar un numero.

            if (metodo == 1){ // prueba lineal.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                L.prueba_lineal_buscar(num_temp,array,n);
                imprime_array(array,n);

            } else if (metodo == 2){ // prueba cuadratica.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                C.prueba_cuadratica_buscar(num_temp, n, array);
                imprime_array(array,n);

            } else if (metodo == 3){ // prueba de direccion.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                D.prueba_direccion_buscar(num_temp, n, array);
                imprime_array(array,n);
            } else{ // encadenamiento.
                cout << "Ingrese un numero: ";
                cin >> num_temp;

                E.buscar(num_temp);
                E.imprimir();
            }

        } else{ //Opcion invalida...
            cout << "Opcion invalida..." << endl;
        }

        cout << endl;
        cout << "Ingrese 1 para continuar" << endl;
        cout << "Ingrese 2 para salir" << endl;
        cout << "   Opcion: ";
        cin >>resp;
        system("clear");
    }
	return 0;
}