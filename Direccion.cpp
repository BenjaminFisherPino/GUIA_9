#include <iostream>
using namespace std;

#include "Direccion.h"

/*CONSTRUCTOR*/
Direccion::Direccion(){}

/*FUNCION HASH*/
int Direccion::hash_modulo(int num, int n){
    int clave = (num % n);
    return clave;
}

/*FUNCION HASH ALTERNATIVA*/
int Direccion::new_hash_modulo(int D){
    int clave = ((D + 1) % 10) + 1;
    return clave;
}

/*BUSCAR UN NUMERO*/
void Direccion::prueba_direccion_buscar(int num, int n, int *array){

    int D, DX;
    D = hash_modulo(num, n);

    // Si la informacion se encuentra en la posicion indicada por 
    // la funcion hash... En caso contrario se procede a buscar
    // dentro de todo el array.
    if ((array[D] != 0) && (array[D] == num)){
        cout << "La informacion esta en la posicion " << D + 1<< endl;
    } else{
        DX = new_hash_modulo(D);

        // Ciclo que nos permite buscar el elemento deseado en el array.
        while((DX <= n) && (array[DX] != 0) && (array[DX] != num) && (DX != D)){
            DX = new_hash_modulo(DX);
        }

        // Si la informacion no se encuentra en el array.
        if ((array[DX] == 0) || (array[DX] != num)){
        cout << "La informacion no se encuentra en el array." << endl;
        } else{
        cout << "La informacion se encuentra en la posicion " << DX + 1 << endl;
        }
    }
}

/*INSERTAR UN NUMERO*/
void Direccion::prueba_direccion_insertar(int num, int n, int *array){

    int D, DX;
    D = hash_modulo(num, n);

    // Si el espacio esta vacio, se le asigna el numero ingresado.
    // En el caso contrario, se procede a buscar un espacio vacio en el array.
    if ((array[D] == 0)){
        array[D] = num;
    } else{
        cout << "El espacio " << D + 1 << " esta ocupado por el numero " << array[D] << endl;

        // Cuando ingresamos un numero que ya esta en el array, la funcion Hash nos va a 
        // arrojar la misma direccion. Por esto, se verifica de que el numero ingresado sea
        // distinto del numero con el cual tuvo una colision, de este modo, no se repiten los
        // los numeros en el array.
        bool verificador = false;

        if (num == array[D]){
            verificador = true;
        }

        DX = new_hash_modulo(D);

        while((DX <= n) && (array[DX] != 0) && (array[DX] != num) && (DX != D) && (!verificador)){
            DX = new_hash_modulo(DX);

            if (num == array[DX]){
                verificador = true;
            }
        }

        if ((array[DX] == 0) && (DX < n)){
            if (verificador){
                cout << "La clave ya se encuentra dentro del array" << endl;
            } else{
                cout << "Se opto por localizar el numero " << num << " en la posicion " << DX + 1 << endl;
                array[DX] = num;
            }
        } else{
            cout << "No queda espacio en el array" << endl;
        }
    }
}