#ifndef CUADRATICA_H
#define CUADRATICA_H

class Cuadratica {

    /*ATRIBUTOS*/
    private:

    public:
    /*CONSTRUCTOR*/
		Cuadratica();
    /*METODOS*/
        int hash_modulo(int num, int n);
        void prueba_cuadratica_buscar(int clave, int n, int *array);
        void prueba_cuadratica_insertar(int clave, int n, int *array);
};
#endif