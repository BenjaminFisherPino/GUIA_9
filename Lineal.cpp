#include <iostream>
using namespace std;

#include "Lineal.h"

/*CONSTRUCTOR  */
Lineal::Lineal(){}

/*FUNCION HASH*/
int Lineal::hash_modulo(int num, int n){
    int clave = (num % n);
    return clave;
}

/*BUSCAR NUMERO*/
void Lineal::prueba_lineal_buscar(int num, int *array, int n){
    int D, DX;

    D = hash_modulo(num, n);

    // Si la informacion se encuentra en la posicion indicada por 
    // la funcion hash... En caso contrario se procede a buscar
    // dentro de todo el array.
    if ((array[D] != 0) && (array[D] == num)){
        cout << "La informacion esta en la posicion " << D + 1<< endl;
    } else{
        DX = D + 1;

        // Ciclo que nos permite buscar el elemento deseado en el array.
        while ((DX <= n) && (array[DX] != 0) && (array[DX] != num) && (DX != D)){

            DX = DX + 1;
            if (DX == (n + 1)){
                DX = 0;
            }
        }

        // Si el ultimo espacio revisado tiene un valor igual a 0 y
        // el valor de DX es igual a D (esto significa que se reviso
        // cada valor dentro del array)... En caso contrario se informa
        // al usuario de que se encntro el numero en el array.
        if ((array[DX] == 0) || ( DX == D)){
            cout << "La informacion no se encuentra en el arbol" << endl;
        } else{
            cout << "La informacion se encuentra en la posicion " << DX + 1 << endl;
        }
    }
}

/*INSERTAR NUMERO*/
void Lineal:: prueba_lineal_insertar(int num, int *array, int n){
    int D, DX;

    D = hash_modulo(num, n);

    // Si la posicion indicada por la funcion Hash esta vacia se inserta el numero.
    // En caso contrario, se empieza a buscar en el array algun lugar vacio.
    if ((array[D] == 0)){
        array[D] = num;
    } else{
        cout << "El espacio " << D + 1 << " esta ocupado por el numero " << array[D] << endl;
        DX = D + 1;
        int sum;

        // Cuando ingresamos un numero que ya esta en el array, la funcion Hash nos va a 
        // arrojar la misma direccion. Por esto, se verifica de que el numero ingresado sea
        // distinto del numero con el cual tuvo una colision, de este modo, no se repiten los
        // los numeros en el array.
        bool verificador = false;
        sum = n + 1;

        /*VERIFICADOR*/
        if (num == array[D]){
            verificador = true;
        }

        // Ciclo que nos permite buscar un espacio vacio en el array.
        while ((DX <= n) && (array[DX] != 0) && (array[DX] != num) && (DX != D) && (!verificador)){
            DX = DX + 1;

            if (DX == sum){
                DX = 0;
            }
        }

        // Si se encontro un espacio vacio.
        if ((array[DX] == 0) && (DX < n)){

            // El numero se repite.
            if (verificador){
                cout << "La clave ya se encuentra dentro del array" << endl;

            // Al numero se le da una nueva direccion.
            } else{
                cout << "Se opto por localizar el numero " << num << " en la posicion " << DX + 1 << endl;
                array[DX] = num;
            }
            
        // No hay espacios vacios en el array.
        } else{
            cout << "No queda espacio en el array" << endl;
        }
    }
}