#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

class Lista {

    public:
		Nodo *principal = NULL;
		
        //CONSTRUCTOR POR DEFECTO
        Lista();
        
        //Se crea un nodo que recibe un objeto tipo numero.
        void agregar(int num);

        //Busca un elemento
        void buscar(int num);
        
        //Imprime la lista actual.
        void imprimir();
};
#endif
