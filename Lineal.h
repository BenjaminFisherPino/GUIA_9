#ifndef LINEAL_H
#define LINEAL_H

class Lineal {

    /*ATRIBUTOS*/
    private:

    public:
    /*CONSTRUCTOR*/
		Lineal();

    /*METODOS*/
        int hash_modulo(int num, int n);
        void prueba_lineal_buscar(int clave, int *array, int n);
        void prueba_lineal_insertar(int clave, int *array, int n);
};
#endif